package edu.example.app;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = StudentHelp.class)
class StudentHelpTests {

    @Test
    void contextLoads() {
    }

}
