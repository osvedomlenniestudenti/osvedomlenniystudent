package edu.example.app.service;

import edu.example.app.dao.repository.SubjectRepository;
import edu.example.app.dao.repository.UserRepository;
import edu.example.app.entity.SubjectEntity;
import edu.example.app.entity.UserEntity;
import edu.example.app.service.domain.SubjectService;
import jakarta.persistence.EntityNotFoundException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class SubjectServiceTest {

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    @AfterEach
    public void clear(){
        subjectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @RepeatedTest(5)
    public void existById() {
        //given
        UserEntity user = createUser();
        SubjectEntity subject = createSubject(user, RandomUtils.nextInt(1,7));

        //when
        boolean existById = subjectService.existById(subject.getId());
        boolean notExistById = subjectService.existById(Long.parseLong(RandomStringUtils.randomNumeric(10)));

        //then
        assertTrue(existById);
        assertFalse(notExistById);
    }

    @RepeatedTest(5)
    public void createSubject() {
        //given
        UserEntity user = createUser();

        SubjectEntity subject = new SubjectEntity();
        subject.setName(RandomStringUtils.random(15));
        subject.setCourse(RandomUtils.nextInt(1,7));
        subject.setAuthor(user);

        //when
        SubjectEntity subjectTaken = subjectService.createSubject(subject);

        //then
        assertEquals(subject.getCourse(), subjectTaken.getCourse());
        assertEquals(subject.getName(), subjectTaken.getName());
        assertEquals(subject.getAuthor().getId(), user.getId());
    }

    @RepeatedTest(5)
    public void deleteSubject(){
        //given
        UserEntity user = createUser();

        SubjectEntity subject = new SubjectEntity();
        subject.setName(RandomStringUtils.random(15));
        subject.setCourse(RandomUtils.nextInt(1,7));
        subject.setAuthor(user);
        subject = subjectRepository.save(subject);
        Long id = subject.getId();

        //when
        subjectService.deleteSubject(id);

        //then
        assertThrows(
                EntityNotFoundException.class,
                () -> subjectService.deleteSubject(id)
        );
    }

    @RepeatedTest(5)
    public void deleteSubjectNotExist(){
        assertThrows(
                EntityNotFoundException.class,
                () -> subjectService.deleteSubject(Long.parseLong(RandomStringUtils.randomNumeric(10)))
        );
    }

    @RepeatedTest(5)
    public void getSubject(){
        //given
        UserEntity user = createUser();

        SubjectEntity subject = new SubjectEntity();
        subject.setName(RandomStringUtils.random(15));
        subject.setCourse(RandomUtils.nextInt(1,7));
        subject.setAuthor(user);
        subject = subjectRepository.save(subject);

        //when
        SubjectEntity subjectTaken = subjectService.getSubject(subject.getId());

        //then
        assertEquals(subject.getCourse(), subjectTaken.getCourse());
        assertEquals(subject.getName(), subjectTaken.getName());
        assertEquals(subject.getAuthor().getId(), user.getId());
    }

    @RepeatedTest(5)
    public void getSubjectNotExist(){
        assertThrows(
                EntityNotFoundException.class,
                () -> subjectService.getSubject(Long.parseLong(RandomStringUtils.randomNumeric(10)))
        );
    }

    private SubjectEntity createSubject(UserEntity user, Integer course){
        SubjectEntity subject = new SubjectEntity();
        subject.setCourse(course);
        subject.setName(RandomStringUtils.random(10));
        subject.setAuthor(user);
        subject = subjectRepository.save(subject);
        return subject;
    }

    private UserEntity createUser() {
        UserEntity user = new UserEntity();
        user.setName(RandomStringUtils.randomAlphabetic(15));
        user.setPasswordHash(RandomStringUtils.randomAlphabetic(10));
        user = userRepository.save(user);
        return user;
    }
}
