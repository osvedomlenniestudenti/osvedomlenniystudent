package edu.example.app.service;

import java.time.LocalDateTime;

import edu.example.app.dao.repository.PostRepository;
import edu.example.app.dao.repository.SubjectRepository;
import edu.example.app.dao.repository.TaskRepository;
import edu.example.app.dao.repository.UserRepository;
import edu.example.app.entity.*;
import edu.example.app.service.domain.PostService;
import jakarta.persistence.EntityNotFoundException;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PostServiceTest {

    @Autowired
    private PostService postService;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    @AfterEach
    public void clear() {
        postRepository.deleteAll();
        taskRepository.deleteAll();
        subjectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @RepeatedTest(5)
    public void existById() {
        //given
        UserEntity user = createUser();
        SubjectEntity subject = createSubject(user);
        TaskEntity task = createTask(user, subject);

        String text = RandomStringUtils.random(100);
        LocalDateTime now = LocalDateTime.now();
        PostEntity post = new PostEntity();
        post.setTitle(RandomStringUtils.random(10));
        post.setText(text);
        post.setTask(task);
        post.setCreatedAt(now);
        post.setLastUpdatedAt(now);
        post.setVersion(1);
        post.setAuthor(user);
        post = postRepository.save(post);

        //when
        boolean existById = postService.existById(post.getId());
        boolean notExistById = postService.existById(Long.parseLong(RandomStringUtils.randomNumeric(10)));

        //then
        assertTrue(existById);
        assertFalse(notExistById);
    }

    @RepeatedTest(5)
    public void createPost(){
        //given
        UserEntity user = createUser();
        SubjectEntity subject = createSubject(user);
        TaskEntity task = createTask(user, subject);

        String text = RandomStringUtils.random(100);
        PostEntity post = new PostEntity();
        post.setTitle(RandomStringUtils.random(10));
        post.setText(text);
        post.setTask(task);
        post.setCreatedAt(LocalDateTime.now());
        post.setLastUpdatedAt(LocalDateTime.now());
        post.setAttachmentPath(RandomStringUtils.random(20));
        post.setVersion(Integer.parseInt(RandomStringUtils.randomNumeric(5)));
        post.setAuthor(user);

        //when
        PostEntity postTaken = postService.createPost(post);

        //then
        assertEquals(postTaken.getTask().getId(), task.getId());
        assertEquals(postTaken.getVersion(), post.getVersion());
        assertEquals(postTaken.getText(), text);
        assertEquals(postTaken.getTitle(), post.getTitle());
        assertEquals(postTaken.getAttachmentPath(), post.getAttachmentPath());
        assertEquals(postTaken.getAuthor().getId(), user.getId());
    }

    @RepeatedTest(5)
    public void createPostWithNotExistTask() {
        //given
        UserEntity user = createUser();
        TaskEntity task = new TaskEntity();
        task.setId(Long.parseLong(RandomStringUtils.randomNumeric(10)));

        String text = RandomStringUtils.random(100);
        PostEntity post = new PostEntity();
        post.setTitle(RandomStringUtils.random(10));
        post.setText(text);
        post.setTask(task);
        post.setCreatedAt(LocalDateTime.now());
        post.setLastUpdatedAt(LocalDateTime.now());
        post.setAttachmentPath(RandomStringUtils.random(20));
        post.setVersion(Integer.parseInt(RandomStringUtils.randomNumeric(5)));
        post.setAuthor(user);

        //then
        assertThrows(
                EntityNotFoundException.class,
                () -> postService.createPost(post)
        );
    }

    @RepeatedTest(5)
    public void deletePost() {
        //given
        UserEntity user = createUser();
        SubjectEntity subject = createSubject(user);
        TaskEntity task = createTask(user, subject);

        String text = RandomStringUtils.random(100);
        PostEntity post = new PostEntity();
        post.setTitle(RandomStringUtils.random(10));
        post.setText(text);
        post.setTask(task);
        post.setCreatedAt(LocalDateTime.now());
        post.setLastUpdatedAt(LocalDateTime.now());
        post.setAttachmentPath(RandomStringUtils.random(20));
        post.setVersion(Integer.parseInt(RandomStringUtils.randomNumeric(5)));
        post.setAuthor(user);
        post = postRepository.save(post);

        //when
        postService.deletePost(post.getId());

        //then
        PostEntity finalPost = post;
        assertThrows(
                EntityNotFoundException.class,
                () -> postRepository.findById(finalPost.getId())
                        .orElseThrow(EntityNotFoundException::new)
        );
    }

    @RepeatedTest(5)
    public void deletePostNotExist(){
        assertThrows(
                EntityNotFoundException.class,
                () -> postService.deletePost(Long.parseLong(RandomStringUtils.randomNumeric(10)))
        );
    }

    @RepeatedTest(5)
    public void getPost() {
        //given
        UserEntity user = createUser();
        SubjectEntity subject = createSubject(user);
        TaskEntity task = createTask(user, subject);

        String text = RandomStringUtils.random(100);
        PostEntity post = new PostEntity();
        post.setTitle(RandomStringUtils.random(10));
        post.setText(text);
        post.setTask(task);
        post.setCreatedAt(LocalDateTime.now());
        post.setLastUpdatedAt(LocalDateTime.now());
        post.setAttachmentPath(RandomStringUtils.random(20));
        post.setVersion(Integer.parseInt(RandomStringUtils.randomNumeric(5)));
        post.setAuthor(user);
        post = postRepository.save(post);

        //when
        PostEntity postTaken = postService.getPost(post.getId());

        //then
        assertEquals(postTaken.getTask().getId(), task.getId());
        assertEquals(postTaken.getVersion(), post.getVersion());
        assertEquals(postTaken.getText(), text);
        assertEquals(postTaken.getTitle(), post.getTitle());
        assertEquals(postTaken.getAttachmentPath(), post.getAttachmentPath());
        assertEquals(postTaken.getAuthor().getId(), user.getId());
    }

    @RepeatedTest(5)
    public void getPostNotExist() {
        assertThrows(
                EntityNotFoundException.class,
                () -> postService.getPost(Long.parseLong(RandomStringUtils.randomNumeric(10)))
        );
    }

    private SubjectEntity createSubject(UserEntity user){
        SubjectEntity subject = new SubjectEntity();
        subject.setCourse(1);
        subject.setName(RandomStringUtils.random(10));
        subject.setAuthor(user);
        subject = subjectRepository.save(subject);
        return subject;
    }

    private TaskEntity createTask(UserEntity user, SubjectEntity subject){
        TaskEntity task = new TaskEntity();

        task.setType(TaskType.ABSTRACT);
        task.setSubject(subject);
        task.setTitle(RandomStringUtils.random(10));
        task.setCreatedAt(LocalDateTime.now());
        task.setAuthor(user);
        task = taskRepository.save(task);
        return task;
    }

    private UserEntity createUser() {
        UserEntity user = new UserEntity();
        user.setName(RandomStringUtils.randomAlphabetic(15));
        user.setPasswordHash(RandomStringUtils.randomAlphabetic(10));
        user = userRepository.save(user);
        return user;
    }
}
