package edu.example.app.service;

import edu.example.app.dao.repository.*;
import edu.example.app.entity.*;
import edu.example.app.service.domain.CommentService;
import jakarta.persistence.EntityNotFoundException;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class CommentServiceTest {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private CommentService commentService;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    @AfterEach
    public void clear(){
        commentRepository.deleteAll();
        postRepository.deleteAll();
        taskRepository.deleteAll();
        subjectRepository.deleteAll();
    }

    @RepeatedTest(5)
    public void existById() {
        //given
        UserEntity user = createUser();
        SubjectEntity subject = createSubject(user);
        TaskEntity task = createTask(user, subject);
        PostEntity post = createPost(user, task);

        String text = RandomStringUtils.random(20);
        LocalDateTime now = LocalDateTime.now();
        var comment = new CommentEntity();
        comment.setText(text);
        comment.setPost(post);
        comment.setCreatedAt(now);
        comment.setAuthor(user);
        comment.setAnonymous(false);
        comment = commentRepository.save(comment);

        //when
        boolean existById = commentService.existById(comment.getId());
        boolean notExistById = commentService.existById(Long.parseLong(RandomStringUtils.randomNumeric(10)));

        //then
        assertTrue(existById);
        assertFalse(notExistById);
    }

    @RepeatedTest(5)
    public void createComment(){
        //given
        UserEntity user = createUser();
        SubjectEntity subject = createSubject(user);
        TaskEntity task = createTask(user, subject);
        PostEntity post = createPost(user, task);

        String text = RandomStringUtils.random(20);
        LocalDateTime now = LocalDateTime.now();
        var comment = new CommentEntity();
        comment.setText(text);
        comment.setPost(post);
        comment.setCreatedAt(now);
        comment.setAuthor(user);
        comment.setAnonymous(false);

        //when
        CommentEntity commentTaken = commentService.createComment(comment);

        //then
        assertEquals(commentTaken.getText(), comment.getText());
        assertEquals(commentTaken.getAuthor().getId(), user.getId());
    }

    @RepeatedTest(5)
    public void createCommentWithNotExistingPost(){
        //given
        UserEntity user = createUser();

        PostEntity post = new PostEntity();
        post.setId(Long.parseLong(RandomStringUtils.randomNumeric(10)));
        String text = RandomStringUtils.random(20);
        LocalDateTime now = LocalDateTime.now();
        var comment = new CommentEntity();
        comment.setText(text);
        comment.setPost(post);
        comment.setCreatedAt(now);
        comment.setAuthor(user);
        comment.setAnonymous(false);

        // then
        assertThrows(
                EntityNotFoundException.class,
                () -> commentService.createComment(comment)
        );
    }


    @RepeatedTest(5)
    public void deleteComment(){
        //given
        UserEntity user = createUser();
        SubjectEntity subject = createSubject(user);
        TaskEntity task = createTask(user, subject);
        PostEntity post = createPost(user, task);

        String text = RandomStringUtils.random(20);
        LocalDateTime now = LocalDateTime.now();
        var comment = new CommentEntity();
        comment.setText(text);
        comment.setPost(post);
        comment.setCreatedAt(now);
        comment.setAuthor(user);
        comment.setAnonymous(false);
        comment = commentRepository.save(comment);

        //when
        commentService.deleteComment(comment.getId());

        //then
        CommentEntity finalComment = comment;
        assertThrows(
                EntityNotFoundException.class,
                () -> commentRepository.findById(finalComment.getId())
                        .orElseThrow(EntityNotFoundException::new)
        );
    }

    @RepeatedTest(5)
    public void deleteCommentNotExist(){
        assertThrows(
                EntityNotFoundException.class,
                () -> commentService.deleteComment(Long.parseLong(RandomStringUtils.randomNumeric(10)))
        );
    }

    @RepeatedTest(5)
    public void getComment(){
        //given
        UserEntity user = createUser();
        SubjectEntity subject = createSubject(user);
        TaskEntity task = createTask(user, subject);
        PostEntity post = createPost(user, task);

        String text = RandomStringUtils.random(20);
        LocalDateTime now = LocalDateTime.now();
        var comment = new CommentEntity();
        comment.setText(text);
        comment.setPost(post);
        comment.setCreatedAt(now);
        comment.setAuthor(user);
        comment.setAnonymous(false);
        comment = commentRepository.save(comment);

        //when
        CommentEntity commentTaken = commentService.getResponseComment(comment.getId());

        //then
        assertEquals(commentTaken.getPost().getId(), post.getId());
        assertEquals(commentTaken.getText(), text);
        assertEquals(commentTaken.getAuthor().getId(), user.getId());
    }

    @RepeatedTest(5)
    public void getCommentNotExist(){
        assertThrows(
                EntityNotFoundException.class,
                () -> commentService.getResponseComment(Long.parseLong(RandomStringUtils.randomNumeric(10)))
        );
    }

    private SubjectEntity createSubject(UserEntity user){
        SubjectEntity subject = new SubjectEntity();
        subject.setCourse(1);
        subject.setName(RandomStringUtils.random(10));
        subject.setAuthor(user);
        subject = subjectRepository.save(subject);
        return subject;
    }

    private TaskEntity createTask(UserEntity user, SubjectEntity subject){
        TaskEntity task = new TaskEntity();
        task.setType(TaskType.ABSTRACT);
        task.setSubject(subject);
        task.setTitle(RandomStringUtils.random(10));
        task.setCreatedAt(LocalDateTime.now());
        task.setAuthor(user);
        task = taskRepository.save(task);
        return task;
    }

    private PostEntity createPost(UserEntity user, TaskEntity task){
        PostEntity post = new PostEntity();
        post.setTitle(RandomStringUtils.random(10));
        post.setText(RandomStringUtils.random(100));
        post.setTask(task);
        post.setCreatedAt(LocalDateTime.now());
        post.setLastUpdatedAt(LocalDateTime.now());
        post.setVersion(1);
        post.setAuthor(user);
        post = postRepository.save(post);
        return post;
    }

    private UserEntity createUser() {
        UserEntity user = new UserEntity();
        user.setName(RandomStringUtils.randomAlphabetic(15));
        user.setPasswordHash(RandomStringUtils.randomAlphabetic(10));
        user = userRepository.save(user);
        return user;
    }
}
