package edu.example.app.service;

import java.time.LocalDateTime;

import edu.example.app.dao.repository.SubjectRepository;
import edu.example.app.dao.repository.TaskRepository;
import edu.example.app.dao.repository.UserRepository;
import edu.example.app.entity.SubjectEntity;
import edu.example.app.entity.TaskEntity;
import edu.example.app.entity.TaskType;
import edu.example.app.entity.UserEntity;
import edu.example.app.service.domain.TaskService;
import jakarta.persistence.EntityNotFoundException;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class TaskServiceTest {

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    @AfterEach
    public void clear(){
        taskRepository.deleteAll();
        subjectRepository.deleteAll();
    }

    @RepeatedTest(5)
    public void existById() {
        //given
        UserEntity user = createUser();
        SubjectEntity subject = createSubject(user);
        String title = RandomStringUtils.random(100);

        TaskEntity task = new TaskEntity();
        task.setSubject(subject);
        task.setTitle(title);
        task.setType(TaskType.TEST);
        task.setCreatedAt(LocalDateTime.now());
        task.setAuthor(user);
        task = taskRepository.save(task);

        //when
        boolean existById = taskService.existById(task.getId());
        boolean notExistById = taskService.existById(Long.parseLong(RandomStringUtils.randomNumeric(10)));

        //then
        assertTrue(existById);
        assertFalse(notExistById);
    }

    @RepeatedTest(5)
    public void createTask(){
        //given
        UserEntity user = createUser();
        SubjectEntity subject = createSubject(user);
        String title = RandomStringUtils.random(100);

        TaskEntity task = new TaskEntity();
        task.setSubject(subject);
        task.setTitle(title);
        task.setType(TaskType.TEST);
        task.setCreatedAt(LocalDateTime.now());
        task.setAuthor(user);

        //when
        TaskEntity taskTaken = taskService.createTask(task);

        //then
        assertEquals(taskTaken.getSubject().getId(), subject.getId());
        assertEquals(taskTaken.getType(), TaskType.TEST);
        assertEquals(taskTaken.getAuthor().getId(), user.getId());
    }

    @RepeatedTest(5)
    public void createTaskWithNotExistSubject(){
        //given
        UserEntity user = createUser();
        String title = RandomStringUtils.random(100);

        SubjectEntity subject = new SubjectEntity();
        subject.setId(Long.parseLong(RandomStringUtils.randomNumeric(10)));
        TaskEntity task = new TaskEntity();
        task.setTitle(title);
        task.setSubject(subject);
        task.setType(TaskType.ABSTRACT);
        task.setCreatedAt(LocalDateTime.now());
        task.setAuthor(user);

        //then
        assertThrows(
                EntityNotFoundException.class,
                () -> taskService.createTask(task)
        );
    }

    @RepeatedTest(5)
    public void deleteTask(){
        //given
        UserEntity user = createUser();
        SubjectEntity subject = createSubject(user);
        String title = RandomStringUtils.random(100);

        TaskEntity task = new TaskEntity();
        task.setTitle(title);
        task.setSubject(subject);
        task.setType(TaskType.TEST);
        task.setCreatedAt(LocalDateTime.now());
        task.setAuthor(user);
        task = taskRepository.save(task);
        Long id = task.getId();

        //when
        taskService.deleteTask(id);

        //then
        assertThrows(
                EntityNotFoundException.class,
                () -> taskService.deleteTask(id)
        );
    }

    @RepeatedTest(5)
    public void deleteTaskNotExist(){
        assertThrows(
                EntityNotFoundException.class,
                () -> taskService.deleteTask(Long.parseLong(RandomStringUtils.randomNumeric(10)))
        );
    }

    @RepeatedTest(5)
    public void getTask(){
        //given
        UserEntity user = createUser();
        SubjectEntity subject = createSubject(user);
        String title = RandomStringUtils.random(100);

        TaskEntity task = new TaskEntity();
        task.setSubject(subject);
        task.setTitle(title);
        task.setType(TaskType.LITERATURE);
        task.setCreatedAt(LocalDateTime.now());
        task.setAuthor(user);
        task = taskRepository.save(task);
        Long id = task.getId();

        //when
        TaskEntity taskTaken = taskService.getTask(id);

        //then
        assertEquals(taskTaken.getSubject().getId(), subject.getId());
        assertEquals(taskTaken.getType(), TaskType.LITERATURE);
        assertEquals(taskTaken.getAuthor().getId(), user.getId());
    }

    @RepeatedTest(5)
    public void getTaskNotExist(){
        assertThrows(
                EntityNotFoundException.class,
                () -> taskService.getTask(Long.parseLong(RandomStringUtils.randomNumeric(10)))
        );
    }

    private SubjectEntity createSubject(UserEntity user){
        SubjectEntity subject = new SubjectEntity();
        subject.setCourse(1);
        subject.setName(RandomStringUtils.random(10));
        subject.setAuthor(user);
        subject = subjectRepository.save(subject);
        return subject;
    }

    private UserEntity createUser() {
        UserEntity user = new UserEntity();
        user.setName(RandomStringUtils.randomAlphabetic(15));
        user.setPasswordHash(RandomStringUtils.randomAlphabetic(10));
        user = userRepository.save(user);
        return user;
    }
}
