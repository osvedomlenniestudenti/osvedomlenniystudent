package edu.example.app.service;

import edu.example.app.dao.repository.UserRepository;
import edu.example.app.entity.UserEntity;
import edu.example.app.service.domain.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    @AfterEach
    public void clear() {
        userRepository.deleteAll();
    }

    @Test
    public void createUser() {
        //given
        var user = new UserEntity();
        user.setName(RandomStringUtils.randomAlphabetic(10));
        user.setPasswordHash(RandomStringUtils.randomAlphabetic(15));
        user.setActive(true);
        user.setHidden(false);
        user.setAge(15);
        user.setEmail("digjfi@mail.ru");

        //when
        UserEntity userTaken = userService.createUser(user);

        //then
        assertEquals(userTaken.getName(), user.getName());
        assertEquals(userTaken.getPassword(), user.getPassword());
        assertEquals(userTaken.isActive(), user.isActive());
        assertEquals(userTaken.isHidden(), user.isHidden());
        assertEquals(userTaken.getAge(), user.getAge());
        assertEquals(userTaken.getEmail(), user.getEmail());
    }

    @Test
    public void updateProfileUser() {
        //given
        var user = new UserEntity();
        user.setName(RandomStringUtils.randomAlphabetic(10));
        user.setPasswordHash(RandomStringUtils.randomAlphabetic(15));
        user.setActive(true);
        user.setHidden(false);
        user.setAge(15);
        user.setEmail("digjfi@mail.ru");
        user = userRepository.save(user);

        var userToUpdate = new UserEntity();
        userToUpdate.setName(user.getName());
        userToUpdate.setAge(45);
        userToUpdate.setEmail("supermail@gmail.com");
        userToUpdate.setDescription("This is description");
        userToUpdate.setTelegram("@telegram");

        //when
        UserEntity userUpdated = userService.updateProfile(userToUpdate);

        //then
        assertEquals(userToUpdate.getName(), userUpdated.getName());
        assertEquals(userToUpdate.getAge(), userUpdated.getAge());
        assertEquals(userToUpdate.getEmail(), userUpdated.getEmail());
        assertEquals(userToUpdate.getDescription(), userUpdated.getDescription());
        assertEquals(userToUpdate.getTelegram(), userUpdated.getTelegram());
    }

    @Test
    public void getUser() {
        //given
        var user = new UserEntity();
        user.setName(RandomStringUtils.randomAlphabetic(10));
        user.setPasswordHash(RandomStringUtils.randomAlphabetic(15));
        user.setActive(true);
        user.setHidden(false);
        user.setAge(15);
        user.setEmail("digjfi@mail.ru");
        user.setDescription("description");
        user.setTelegram("@telegram");
        user = userRepository.save(user);
        String username = user.getUsername();

        //when
        UserEntity userTaken = userService.getUserByUsername(username);

        //then
        assertEquals(userTaken.getName(), user.getName());
        assertEquals(userTaken.getPassword(), user.getPassword());
        assertEquals(userTaken.isActive(), user.isActive());
        assertEquals(userTaken.isHidden(), user.isHidden());
        assertEquals(userTaken.getAge(), user.getAge());
        assertEquals(userTaken.getEmail(), user.getEmail());
        assertEquals(userTaken.getDescription(), user.getDescription());
        assertEquals(userTaken.getTelegram(), user.getTelegram());
    }

    @Test
    public void getUserNotExists() {
        assertThrows(
                UsernameNotFoundException.class,
                () -> userService.getUserByUsername(RandomStringUtils.randomNumeric(10))
        );
    }
}
