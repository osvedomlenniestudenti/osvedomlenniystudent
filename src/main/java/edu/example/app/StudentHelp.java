package edu.example.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentHelp {

    public static void main(String[] args) {
        SpringApplication.run(StudentHelp.class, args);
    }

}
