package edu.example.app.service.scheduler;

import edu.example.app.entity.specification.CommentSpecifications;
import edu.example.app.service.domain.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


@Service
@RequiredArgsConstructor
public class DeleteOldCommentScheduler {

    private final int DeleteOldCommentSchedulerRateTime = 1000 * 60 * 15;
    private final CommentService commentService;
    @Value("${app.job.comments-obsolescence-time}")
    private Long commentsObsolescenceTime;


    @Scheduled(fixedRate = DeleteOldCommentSchedulerRateTime)
    @Async
    public void execute() {
        System.out.println("Cleared");
        var res = LocalDateTime.now().minusSeconds(commentsObsolescenceTime);
        commentService.deleteComments(CommentSpecifications.createBefore(res));
    }


}
