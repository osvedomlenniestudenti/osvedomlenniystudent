package edu.example.app.service.domain;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Random;

import edu.example.app.entity.FilePropertyEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class FileService {

    private final FilePropertyService filePropertyService;
    private final Random random = new Random();
    @Value("${app.file-system.file-root}")
    private String root;

    @Transactional
    public FilePropertyEntity saveFile(MultipartFile file) throws IOException {
        Path path = Path.of(root);
        var property = new FilePropertyEntity();
        property.setName(file.getName());
        property.setPath(property.getName() + random.nextLong());
        Path destinationFile = path.resolve(property.getPath()).normalize().toAbsolutePath();
        try (InputStream inputStream = file.getInputStream()) {
            Files.copy(inputStream, destinationFile,
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            Files.delete(destinationFile);
            throw new IOException();
        }
        return filePropertyService.createFileProperty(property);
    }

    public Resource getFileByName(String name) throws FileNotFoundException, MalformedURLException {
        var property = filePropertyService.getFilePropertyByName(name);
        Path path = Path.of(root);
        return new UrlResource(path.resolve(property.getPath()).toUri());
    }
}
