package edu.example.app.service.domain;

import java.time.LocalDateTime;

import edu.example.app.dao.repository.PostRepository;
import edu.example.app.entity.PostEntity;
import edu.example.app.entity.RoleEntity;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.naming.NoPermissionException;

import static edu.example.app.entity.TaskEntity.FEED_ID;
import static edu.example.app.entity.security.Roles.ADMIN;
import static edu.example.app.entity.security.Roles.MODERATOR;

@Service
@RequiredArgsConstructor
public final class PostService {
    private final PostRepository postRepository;
    private final TaskService taskService;
    private final UserService userService;

    public boolean existById(Long id) {
        return postRepository.existsById(id);
    }
    public PostEntity createPost(PostEntity post){

        if (!taskService.existById(post.getTask().getId())) {
            throw new EntityNotFoundException("task with given id doesnt exist");
        }

        var user = userService.getUserByUsername(post.getAuthor().getName());
        post.setAuthor(user);

        post.setCreatedAt(LocalDateTime.now());
        post.setLastUpdatedAt(LocalDateTime.now());
        post.setVersion(0);
        return postRepository.save(post);
    }

    public void deletePost(Long id) {
        if (!existById(id)) {
            throw new EntityNotFoundException("post with given id doesnt exist");
        }
        postRepository.deleteById(id);
    }

    public PostEntity getPost(Long id) {
        if (!existById(id)) {
            throw new EntityNotFoundException("post with given id doesnt exist");
        }
        return postRepository.findById(id).get();
    }


    public Page<PostEntity> getPosts(Specification<PostEntity> specification, Pageable pageable) {
        return postRepository.findAll(specification, pageable);
    }
}