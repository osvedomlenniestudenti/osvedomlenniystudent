package edu.example.app.service.domain;

import edu.example.app.dao.repository.SubjectRepository;
import edu.example.app.entity.SubjectEntity;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public final class SubjectService {
    private final SubjectRepository subjectRepository;
    private final UserService userService;

    public boolean existById(Long id) {
        return subjectRepository.existsById(id);
    }

    public SubjectEntity createSubject(SubjectEntity subject) throws EntityExistsException {
        var subjectName = subject.getName();
        var subjectCourse = subject.getCourse();
        if (subjectRepository.existsByNameAndCourse(subjectName, subjectCourse)) {
            throw new EntityExistsException("subject with given name and course already exists");
        }

        String userName = subject.getAuthor().getName();
        var user = userService.getUserByUsername(userName);
        subject.setAuthor(user);
        return subjectRepository.save(subject);
    }

    public void deleteSubject(Long id) throws EntityNotFoundException {
        if (!existById(id)) {
            throw new EntityNotFoundException("subject with given id doesnt exist");
        }
        subjectRepository.deleteById(id);
    }

    public SubjectEntity getSubject(Long id) throws EntityNotFoundException {
        if (!existById(id)) {
            throw new EntityNotFoundException("subject with given id doesnt exist");
        }
        return subjectRepository.findById(id).get();
    }

    public Page<SubjectEntity> getSubjects(Specification<SubjectEntity> specification, Pageable pageable) {
        return subjectRepository.findAll(specification, pageable);
    }
}
