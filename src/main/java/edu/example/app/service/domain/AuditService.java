package edu.example.app.service.domain;

import edu.example.app.dao.repository.AuditRepository;
import edu.example.app.entity.AuditEntity;
import edu.example.app.entity.CommentEntity;
import edu.example.app.mapper.AuditMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AuditService {
    private final AuditRepository auditRepository;
    private final AuditMapper auditMapper;

    @Transactional
    public void audit(CommentEntity comment) {
        AuditEntity audit = auditMapper.toEntity(comment);
        auditRepository.save(audit);
    }

    public Page<AuditEntity> getAudits(Specification<AuditEntity> specification, PageRequest pageRequest) {
        return auditRepository.findAll(specification, pageRequest);
    }
}
