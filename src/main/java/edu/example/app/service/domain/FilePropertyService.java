package edu.example.app.service.domain;

import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;

import edu.example.app.dao.repository.FilePropertyRepository;
import edu.example.app.entity.FilePropertyEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class FilePropertyService {

    private final FilePropertyRepository filePropertyRepository;

    @Transactional
    public FilePropertyEntity createFileProperty(FilePropertyEntity fileProperty) throws FileAlreadyExistsException {
        if (filePropertyRepository.existsByName(fileProperty.getName())) {
            throw new FileAlreadyExistsException("file with given name already exist");
        }
        return filePropertyRepository.save(fileProperty);
    }

    public FilePropertyEntity getFilePropertyByName(String name) throws FileNotFoundException {

        if (!filePropertyRepository.existsByName(name)) {
            throw new FileNotFoundException("file with given name doesnt exist");
        }
        return filePropertyRepository.findFilePropertiesByName(name);
    }

}
