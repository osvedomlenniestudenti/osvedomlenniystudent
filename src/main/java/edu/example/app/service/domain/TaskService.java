package edu.example.app.service.domain;

import java.time.LocalDateTime;

import edu.example.app.dao.repository.TaskRepository;
import edu.example.app.entity.TaskEntity;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public final class TaskService {

    private final TaskRepository taskRepository;
    private final SubjectService subjectService;
    private final UserService userService;

    public boolean existById(Long id) {
        return taskRepository.existsById(id);
    }

    public TaskEntity createTask(TaskEntity task){

        var subjectId = task.getSubject().getId();
        if (!subjectService.existById(subjectId)) {
            throw new EntityNotFoundException("subject with given id doesnt exist");
        }
        var taskTitle = task.getTitle();
        if (taskRepository.existsByTitleAndSubjectId(taskTitle, subjectId)) {
            throw new EntityExistsException("task with given title and subjectId already exists");
        }

        var user = userService.getUserByUsername(task.getAuthor().getUsername());
        task.setAuthor(user);

        task.setCreatedAt(LocalDateTime.now());
        return taskRepository.save(task);
    }

    public void deleteTask(Long id) {
        if (!existById(id)) {
            throw new EntityNotFoundException("task with given id doesnt exist");
        }
        taskRepository.deleteById(id);
    }

    public TaskEntity getTask(Long id) {
        if (!existById(id)) {
            throw new EntityNotFoundException("task with given id doesnt exist");
        }
        return taskRepository.findById(id).get();
    }

    public Page<TaskEntity> getTasks(Specification<TaskEntity> specification, Pageable pageable) {
        return taskRepository.findAll(specification, pageable);
    }
}
