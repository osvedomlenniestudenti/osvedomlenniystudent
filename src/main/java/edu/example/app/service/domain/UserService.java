package edu.example.app.service.domain;

import edu.example.app.entity.security.Roles;
import edu.example.app.dao.repository.UserRepository;
import edu.example.app.entity.UserEntity;
import jakarta.persistence.EntityExistsException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Service
public final class UserService {
    private final UserRepository userRepository;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;

    public UserEntity createUser(UserEntity user) throws EntityExistsException {
        String username = user.getName();
        var res = userRepository.findUserByName(username);
        if (res.isPresent()) {
            throw new EntityExistsException("user with given name already exist");
        }

        String password = user.getPassword();
        user.setPasswordHash(passwordEncoder.encode(password));
        user.setRoles(Collections.singleton(roleService.getByName(Roles.USER)));
        user.setActive(true);
        user.setHidden(false);
        return userRepository.save(user);
    }

    public List<UserEntity> getAllUsers() {
        return userRepository.findAllByActiveIsTrue();
    }

    public UserEntity getUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository
                .findUserByName(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
    }

    public UserEntity updateProfile(UserEntity userUpdate) throws UsernameNotFoundException {
        String username = userUpdate.getName();
        var user = userRepository
                .findUserByName(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));

        user.setAge(userUpdate.getAge());
        user.setDescription(userUpdate.getDescription());
        user.setEmail(userUpdate.getEmail());
        user.setTelegram(userUpdate.getTelegram());

        return userRepository.save(user);
    }

    public void deactivateByUsername(String username) {
        var user = userRepository
                .findUserByName(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        user.setActive(false);
        userRepository.save(user);
    }

    public UserEntity addModeratorRole(UserEntity user) {
        var moderRole = roleService.getByName(Roles.MODERATOR);
        user.getRoles().add(moderRole);
        return userRepository.save(user);
    }
}
