package edu.example.app.service.domain;

import edu.example.app.dao.repository.RoleRepository;
import edu.example.app.entity.RoleEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class RoleService {

    private final RoleRepository roleRepository;

    public RoleEntity getByName(String name) {
        return roleRepository.findByName(name);
    }
}
