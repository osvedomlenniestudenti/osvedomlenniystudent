package edu.example.app.service.domain;

import edu.example.app.dao.repository.CommentRepository;
import edu.example.app.entity.CommentEntity;
import edu.example.app.entity.UserEntity;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;
    private final PostService postService;
    private final UserService userService;
    private final AuditService auditService;

    public boolean existById(Long id) {
        return commentRepository.existsById(id);
    }

    @Transactional
    public void createComment(CommentEntity comment) {

        if (!postService.existById(comment.getPost().getId())) {
            throw new EntityNotFoundException("post with given id doesnt exist");
        }

        var user = userService.getUserByUsername(comment.getAuthor().getUsername());
        comment.setAuthor(user);

        CommentEntity parent = null;
        var parentId = comment.getParent().getId();
        if (parentId != null) {
            parent = getComment(parentId);
        }
        comment.setParent(parent);

        comment = commentRepository.save(comment);
        auditService.audit(comment);
    }
    public CommentEntity getComment(Long id) {
        if (!existById(id)) {
            throw new EntityNotFoundException("comment with given id doesnt exist");
        }
        return  commentRepository.findById(id).get();
    }
    public CommentEntity getResponseComment(Long id) {
        CommentEntity comment = getComment(id);
        return checkAnonymous(comment);
    }

    public Page<CommentEntity> getResponseComments(Specification<CommentEntity> specification, Pageable pageable) {
        Page<CommentEntity> page = commentRepository.findAll(specification, pageable);
        page.getContent().forEach(this::checkAnonymous);
        return page;
    }

    @Transactional
    public void updateComment(Long id, String newText) {
        CommentEntity comment = getComment(id);
        comment.setText(newText);
        comment.setUpdatedAt(LocalDateTime.now());
        commentRepository.save(comment);
        auditService.audit(comment);
        getResponseComment(id);
    }

    @Transactional
    public void deleteComment(Long id) {
        if (!existById(id)) {
            throw new EntityNotFoundException("comment with given id doesnt exist");
        }
        commentRepository.deleteById(id);
    }

    @Transactional
    public void deleteComments(Specification<CommentEntity> specification) {
        commentRepository.delete(specification);
    }

    private CommentEntity checkAnonymous(CommentEntity comment){
        if(comment.getAnonymous()){
            comment.setAuthor(new UserEntity("Anonymous"));
        }
        return comment;
    }
}
