package edu.example.app.validation;

import edu.example.app.entity.TaskType;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class TaskTypeValidator implements ConstraintValidator<TaskTypeConstraint, String> {
    @Override
    public boolean isValid(String taskType, ConstraintValidatorContext constraintValidatorContext) {
        if (taskType == null) {
            return true;
        }
        try {
            var temp = TaskType.valueOf(taskType);
            return true;
        } catch (Exception e) {
            constraintValidatorContext.buildConstraintViolationWithTemplate("Указанного типа задания не существует")
                    .addConstraintViolation();
            return false;
        }
    }
}
