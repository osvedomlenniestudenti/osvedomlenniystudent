package edu.example.app.validation;

import edu.example.app.entity.ClassNames;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class ClassNameValidator implements ConstraintValidator<ClassNameConstraint, String> {
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null) {
            return true;
        }
        try {
            ClassNames.valueOf(s);
            return true;
        }
        catch (Exception exception) {
            constraintValidatorContext
                    .buildConstraintViolationWithTemplate("Указанного типа задания не существует")
                    .addConstraintViolation();
            return false;
        }
    }
}
