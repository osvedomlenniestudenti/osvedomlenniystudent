package edu.example.app.dto.user;

import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import static edu.example.app.entity.UserEntity.*;

@Getter
@Setter
public class CreateUserDto {

    @NotBlank(message = "имя должно быть указанно")
    @Size(min = MIN_NAME_LEN, max = MAX_NAME_LEN,
            message = "размер имени должен быть от " + MIN_NAME_LEN + " до " + MAX_NAME_LEN)
    private String name;

    @Size(min = MIN_PASSWORD_LEN, max = MAX_PASSWORD_LEN,
            message = "размер пароля должен быть от " + MIN_PASSWORD_LEN + " до " + MAX_PASSWORD_LEN)
    @Pattern(regexp = ".*\\d.*", message = "пароль должен содержать цифру букву")
    @Pattern(regexp = ".*[A-Z].*", message = "пароль должен содержать заглавную ")
    private String password;

    @Range(min = AGE_MIN, max = AGE_MAX,
            message = "Возраст должен быть в диапазоне от " + AGE_MIN + " до " + AGE_MAX)
    @NotNull(message = "Отсутствует возраст")
    private Integer age;

    @Size(max = DESC_MAX_LEN,
            message = "Слишком длинное описание. Макс длина " + DESC_MAX_LEN)
    private String description;

    @Size(max = EMAIL_MAX_LEN,
            message = "Слишком длинная почта. Макс длина " + EMAIL_MAX_LEN)
    @NotBlank(message = "Отсутствует эл. почта")
    @Email(message = "Некорректный E-Mail")
    private String email;

    @Size(max = TELEGRAM_MAX_LEN,
            message = "Слишком длинный Телеграм. Макс длина " + TELEGRAM_MAX_LEN)
    private String telegram;
}
