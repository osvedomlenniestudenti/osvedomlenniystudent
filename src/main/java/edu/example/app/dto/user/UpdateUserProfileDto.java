package edu.example.app.dto.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import static edu.example.app.entity.UserEntity.*;

@Getter
@Setter
public class UpdateUserProfileDto {

    @NotBlank(message = "Отсутствует имя пользователя")
    private String name;

    @Range(min = AGE_MIN, max = AGE_MAX,
            message = "Возраст должен быть в диапазоне от " + AGE_MIN + " до " + AGE_MAX)
    @NotNull(message = "Отсутствует возраст")
    private Integer age;

    @Size(max = DESC_MAX_LEN,
            message = "Слишком длинное описание. Макс длина " + DESC_MAX_LEN)
    private String description;

    @Size(max = EMAIL_MAX_LEN,
            message = "Слишком длинная почта. Макс длина " + EMAIL_MAX_LEN)
    @NotBlank(message = "Отсутствует эл. почта")
    @Email(message = "Некорректный E-Mail")
    private String email;

    @Size(max = TELEGRAM_MAX_LEN,
            message = "Слишком длинный Телеграм. Макс длина " + TELEGRAM_MAX_LEN)
    private String telegram;

    @NotNull(message = "Отсутствует приватность профиля")
    private boolean hidden;
}
