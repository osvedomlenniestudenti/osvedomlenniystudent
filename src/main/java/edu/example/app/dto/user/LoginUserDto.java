package edu.example.app.dto.user;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import static edu.example.app.entity.UserEntity.*;

@Getter
@Setter
public class LoginUserDto {

    @NotBlank(message = "имя должно быть указанно")
    @Size(min = MIN_NAME_LEN, max = MAX_NAME_LEN,
            message = "размер имени должен быть от " + MIN_NAME_LEN + " до " + MAX_NAME_LEN)
    private String name;

    @NotBlank(message = "пароль должен быть указанно")
    @Size(min = MIN_PASSWORD_LEN, max = MAX_PASSWORD_LEN,
            message = "размер пароля должен быть от " + MIN_PASSWORD_LEN + " до " + MAX_PASSWORD_LEN)
    private String password;
}
