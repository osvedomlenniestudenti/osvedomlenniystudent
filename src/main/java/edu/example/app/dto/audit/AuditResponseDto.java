package edu.example.app.dto.audit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AuditResponseDto {
    private Long entityId;
    private String entityClass;
    private String auditMessage;
    private LocalDateTime createdAt;
}
