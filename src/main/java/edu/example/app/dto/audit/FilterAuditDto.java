package edu.example.app.dto.audit;

import edu.example.app.validation.ClassNameConstraint;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FilterAuditDto {

    @ClassNameConstraint
    private String className;
    private Long entityId;

    private static final int MAX_PAGE_SIZE = 50, DEF_PAGE_SIZE = 20;
    @Max(value = MAX_PAGE_SIZE, message = "максимальный размер страницы " + MAX_PAGE_SIZE)
    @Positive(message = "Размер страницы должен быть позитивным")
    private Integer pageSize = DEF_PAGE_SIZE;

    @NotNull(message = "номер страницы должен быть указан")
    private Integer pageNumber;
}
