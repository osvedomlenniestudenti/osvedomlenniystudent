package edu.example.app.dto.task;

import edu.example.app.validation.TaskTypeConstraint;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static edu.example.app.entity.TaskEntity.MAX_TASK_TITLE_SIZE;
import static edu.example.app.entity.TaskEntity.MAX_TASK_TYPE_SIZE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateTaskDto {

    @NotNull(message = "Id предмета должен быть не пустым")
    private Long subjectId;

    @NotBlank(message = "Название задачи должно содержать хотя бы один не пробельный символ")
    @Size(max = MAX_TASK_TITLE_SIZE,
            message = "Название задачи должно быть меньше " + MAX_TASK_TITLE_SIZE + " символов")
    private String title;

    @NotBlank(message = "Тип задачи должен содержать хотя бы один не пробельный символ")
    @Size(max = MAX_TASK_TYPE_SIZE,
            message = "Тип задачи должен быть меньше " + MAX_TASK_TYPE_SIZE + " символов")
    @TaskTypeConstraint(message = "Тип задачи должен быть одним из трех: \"TEST\", "
            + "\"ABSTRACT\", \"LITERATURE\" ")
    private String type;
}
