package edu.example.app.dto.subject;

import com.fasterxml.jackson.annotation.JsonProperty;
import edu.example.app.dto.user.UserResponseDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubjectResponseDto {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("course")
    private Integer course;

    @JsonProperty("author")
    private UserResponseDto author;
}
