package edu.example.app.dto.subject;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import static edu.example.app.entity.SubjectEntity.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateSubjectDto {

    @NotBlank(message = "Имя предмета должно содержать хотя бы один не пробельный символ")
    @Size(max = MAX_Subject_NAME_SIZE,
            message = "Имя предмета должно быть меньше " + MAX_Subject_NAME_SIZE + " символов")
    private String name;

    @NotNull(message = "Номер курса должен быть не пустым")
    @Range(min = MIN_COURSE, max = MAX_COURSE,
            message = "Номер курса должен быть от " + MIN_COURSE + " до " + MAX_COURSE)
    private Integer course;
}
