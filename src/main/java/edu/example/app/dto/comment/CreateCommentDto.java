package edu.example.app.dto.comment;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static edu.example.app.entity.CommentEntity.MAX_COMMENT_TEXT_SIZE;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateCommentDto {

    @NotBlank(message = "Комментарий должен содержать хотя бы 1 не пробельный символ")
    @Size(max = MAX_COMMENT_TEXT_SIZE,
            message = "Комментарий должен быть меньше " + MAX_COMMENT_TEXT_SIZE + " символов")
    private String text;

    @NotNull(message = "Id поста должен быть не пустым")
    @Positive(message = "Id поста должен быть больше 0")
    private Long postId;

    @NotNull(message = "Анонимность должна быть true или false")
    private Boolean anonymous = false;

    @Positive(message = "Id родительского комментария должен быть больше 0")
    private Long parentId;

}
