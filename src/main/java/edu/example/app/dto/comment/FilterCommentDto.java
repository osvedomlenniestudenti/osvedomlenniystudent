package edu.example.app.dto.comment;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class FilterCommentDto {

    @NotNull(message = "Должен быть указан id поста")
    private Long postId;

    private Long parentId;

    private LocalDateTime createAfter;

    private static final int MAX_PAGE_SIZE = 50, DEF_PAGE_SIZE = 20;
    @Max(value = MAX_PAGE_SIZE, message = "максимальный размер страницы " + MAX_PAGE_SIZE)
    @Positive(message = "Размер страницы должен быть позитивным")
    private Integer pageSize = DEF_PAGE_SIZE;

    @NotNull(message = "номер страницы должен быть указан")
    private Integer pageNumber;
}
