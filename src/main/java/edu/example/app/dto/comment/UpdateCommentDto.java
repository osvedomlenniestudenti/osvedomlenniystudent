package edu.example.app.dto.comment;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import static edu.example.app.entity.CommentEntity.MAX_COMMENT_TEXT_SIZE;

@Getter
@Setter
public class UpdateCommentDto {

    @NotBlank(message = "Комментарий должен содержать хотя бы 1 не пробельный символ")
    @Size(max = MAX_COMMENT_TEXT_SIZE,
            message = "Комментарий должен быть меньше " + MAX_COMMENT_TEXT_SIZE + " символов")
    private String text;
}
