package edu.example.app.dto.post;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

import static edu.example.app.entity.TaskEntity.FEED_ID;

@Getter
@Setter
public class FilterPostDto {

    private LocalDateTime createAfter;

    private Long taskId = Long.parseLong(FEED_ID);

    private static final int MAX_PAGE_SIZE = 50, DEF_PAGE_SIZE = 20;
    @Max(value = MAX_PAGE_SIZE, message = "максимальный размер страницы " + MAX_PAGE_SIZE)
    @Positive(message = "Размер страницы должен быть позитивным")
    private Integer pageSize = DEF_PAGE_SIZE;

    @NotNull(message = "номер страницы должен быть указан")
    private Integer pageNumber;
}
