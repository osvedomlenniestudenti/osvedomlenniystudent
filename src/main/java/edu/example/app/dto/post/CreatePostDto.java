package edu.example.app.dto.post;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static edu.example.app.entity.PostEntity.MAX_POST_TEXT_SIZE;
import static edu.example.app.entity.PostEntity.MAX_POST_TITLE_SIZE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreatePostDto {

    @NotNull(message = "Id задания должен быть не пустым")
    private Long taskId;

    @NotBlank(message = "Название поста должно содержать хотя бы 1 не пробельный символ")
    @Size(max = MAX_POST_TITLE_SIZE,
            message = "Название поста должно быть меньше " + MAX_POST_TITLE_SIZE + " символов")
    private String title;

    @Size(max = MAX_POST_TEXT_SIZE,
            message = "Текст поста должен быть меньше " + MAX_POST_TEXT_SIZE + " символов")
    private String text;
}
