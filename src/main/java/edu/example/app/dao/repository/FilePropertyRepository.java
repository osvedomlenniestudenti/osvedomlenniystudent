package edu.example.app.dao.repository;

import edu.example.app.entity.FilePropertyEntity;
import org.springframework.data.repository.CrudRepository;

public interface FilePropertyRepository extends CrudRepository<FilePropertyEntity, Long> {
    FilePropertyEntity findFilePropertiesByName(String name);

    boolean existsByName(String name);
}
