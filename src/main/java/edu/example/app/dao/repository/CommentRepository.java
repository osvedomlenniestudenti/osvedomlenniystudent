package edu.example.app.dao.repository;

import edu.example.app.entity.CommentEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<CommentEntity, Long>, JpaSpecificationExecutor<CommentEntity> {
}
