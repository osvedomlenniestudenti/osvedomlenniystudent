package edu.example.app.dao.repository;

import java.util.List;
import java.util.Optional;

import edu.example.app.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findUserByName(String name);

    List<UserEntity> findAllByActiveIsTrue();
}
