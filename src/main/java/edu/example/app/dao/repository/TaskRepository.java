package edu.example.app.dao.repository;

import edu.example.app.entity.TaskEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface TaskRepository extends CrudRepository<TaskEntity, Long>, JpaSpecificationExecutor<TaskEntity> {
    boolean existsByTitleAndSubjectId(String title, Long subjectId);
}
