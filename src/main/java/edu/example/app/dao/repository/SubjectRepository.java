package edu.example.app.dao.repository;

import edu.example.app.entity.SubjectEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface SubjectRepository extends CrudRepository<SubjectEntity, Long>, JpaSpecificationExecutor<SubjectEntity> {
    boolean existsByNameAndCourse(String name, Integer course);
}
