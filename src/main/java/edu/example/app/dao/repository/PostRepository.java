package edu.example.app.dao.repository;

import edu.example.app.entity.PostEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<PostEntity, Long>, JpaSpecificationExecutor<PostEntity> {
}
