package edu.example.app.dao.repository;

import edu.example.app.entity.AuditEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface AuditRepository extends CrudRepository<AuditEntity, Long>, JpaSpecificationExecutor<AuditEntity> {

}
