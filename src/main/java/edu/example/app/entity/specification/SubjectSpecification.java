package edu.example.app.entity.specification;

import edu.example.app.entity.SubjectEntity;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;

public abstract class SubjectSpecification {

    static public Specification<SubjectEntity> hasCourse(Integer course) {
        if (Objects.isNull(course)) {
            return (root, query, criteriaBuilder) -> criteriaBuilder.and();
        }
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("course"), course);
    }

    static public Specification<SubjectEntity> notFeed() {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.notEqual(root.get("name"), "feed");
    }
}
