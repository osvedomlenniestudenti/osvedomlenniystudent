package edu.example.app.entity.specification;

import edu.example.app.entity.CommentEntity;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;
import java.util.Objects;

public abstract class CommentSpecifications {

    static public Specification<CommentEntity> createAfter(LocalDateTime date) {

        if (Objects.isNull(date)) {
            return (root, query, criteriaBuilder) -> criteriaBuilder.and();
        }
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.greaterThan(root.get("createdAt"), date);
    }

    static public Specification<CommentEntity> createBefore(LocalDateTime date) {

        if (Objects.isNull(date)) {
            return (root, query, criteriaBuilder) -> criteriaBuilder.and();
        }
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.lessThan(root.get("createdAt"), date);
    }

    static public Specification<CommentEntity> hasPost(Long id) {

        if (Objects.isNull(id)) {
            return ((root, query, criteriaBuilder) -> criteriaBuilder.and());
        }
        return ((root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("post").get("id"), id));
    }

    static public Specification<CommentEntity> hasParent(Long id) {

        if (Objects.isNull(id)) {
            return ((root, query, criteriaBuilder) -> root.get("parent").get("id").isNull());
        }
        return ((root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("parent").get("id"), id));
    }

}
