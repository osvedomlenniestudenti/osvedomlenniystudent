package edu.example.app.entity.specification;

import edu.example.app.entity.TaskEntity;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;
import java.util.Objects;

import static edu.example.app.entity.TaskEntity.FEED_ID;


public abstract class TaskSpecification {
    static public Specification<TaskEntity> createAfter(LocalDateTime date) {

        if (Objects.isNull(date)) {
            return (root, query, criteriaBuilder) -> criteriaBuilder.and();
        }
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.greaterThan(root.get("createdAt"), date);
    }

    static public Specification<TaskEntity> hasSubjectId(Long id) {

        if (Objects.isNull(id)) {
            return (root, query, criteriaBuilder) -> criteriaBuilder.and();
        }
        return ((root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("subject").get("id"), id));
    }

    public static Specification<TaskEntity> notFeed() {
        return ((root, query, criteriaBuilder) ->
                criteriaBuilder.notEqual(root.get("id"), Long.parseLong(FEED_ID)));
    }
}
