package edu.example.app.entity.specification;

import edu.example.app.entity.AuditEntity;
import edu.example.app.entity.ClassNames;
import org.springframework.data.jpa.domain.Specification;

public abstract class AuditSpecification {



    static public Specification<AuditEntity> hasEntityClass(ClassNames classNames) {

        if (classNames == null) {
            return (root, query, criteriaBuilder) -> criteriaBuilder.and();
        }

        return ((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("entityClass"), classNames.name));
    }

    static public Specification<AuditEntity> hasEntityId(Long id) {
        if (id == null) {
            return (root, query, criteriaBuilder) -> criteriaBuilder.and();
        }

        return ((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("entityId"), id));
    }
}
