package edu.example.app.entity.specification;

import edu.example.app.entity.PostEntity;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;
import java.util.Objects;

public abstract class PostSpecifications {
    static public Specification<PostEntity> createAfter(LocalDateTime date) {

        if (Objects.isNull(date)) {
            return (root, query, criteriaBuilder) -> criteriaBuilder.and();
        }
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.greaterThan(root.get("createdAt"), date);
    }
    static public Specification<PostEntity> hasTask(Long id) {

        if (Objects.isNull(id)) {
            return ((root, query, criteriaBuilder) -> criteriaBuilder.and());
        }
        return ((root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("task").get("id"), id));
    }
}
