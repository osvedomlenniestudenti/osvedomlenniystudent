package edu.example.app.entity;


import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "osved_user")
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@EntityListeners({AuditingEntityListener.class})
public class UserEntity implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "osved_user_id_seq")
    @SequenceGenerator(name = "osved_user_id_seq", sequenceName = "osved_user_id_seq", allocationSize = 1)
    @EqualsAndHashCode.Include
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private Integer age;

    @Column(name = "description")
    private String description;

    @Column(name = "email")
    private String email;

    @Column(name = "telegram")
    private String telegram;

    @Column(name = "hidden")
    private boolean hidden;

    @Column(name = "active")
    private boolean active;

    @ToString.Exclude
    @Column(name = "password_hash")
    private String passwordHash;

    @ToString.Exclude
    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    private List<SubjectEntity> subjectList;

    @ToString.Exclude
    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    private List<TaskEntity> tasksList;

    @ToString.Exclude
    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    private List<PostEntity> postList;

    @ToString.Exclude
    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    private List<CommentEntity> commentList;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id")
    )
    private Set<RoleEntity> roles;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getPassword() {
        return passwordHash;
    }

    @Override
    public String getUsername() {
        return name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return active;
    }

    @Override
    public boolean isAccountNonLocked() {
        return active;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return active;
    }

    @Override
    public boolean isEnabled() {
        return active;
    }

    public UserEntity(String name) {
        this.name = name;
    }

    public static final int AGE_MIN = 6, AGE_MAX = 150, DESC_MAX_LEN = 255,
            EMAIL_MAX_LEN = 100, TELEGRAM_MAX_LEN = 100;
    public static final int MIN_NAME_LEN = 5, MAX_NAME_LEN = 32;
    public static final int MIN_PASSWORD_LEN = 10;
    public static final int MAX_PASSWORD_LEN = 100;
}
