package edu.example.app.entity;

import lombok.Getter;

@Getter
public enum TaskType {

    TEST("Контрольная работа"),
    ABSTRACT("Конспект семинаров"),
    LITERATURE("Литература"),
    EXAM("Экзамен"),
    FEED("Лента");

    private final String description;

    TaskType(String description) {
        this.description = description;
    }
}
