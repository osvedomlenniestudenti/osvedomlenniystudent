package edu.example.app.entity;

import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "file_property")
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class FilePropertyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "file_property_id_seq")
    @SequenceGenerator(name = "file_property_id_seq", sequenceName = "file_property_id_seq", allocationSize = 1)
    @EqualsAndHashCode.Include
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "path")
    private String path;
}
