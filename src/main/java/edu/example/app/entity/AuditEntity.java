package edu.example.app.entity;

import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;

@Entity
@Table(name = "audit")
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class AuditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "audit_id_seq")
    @SequenceGenerator(name = "audit_id_seq", sequenceName = "audit_id_seq", allocationSize = 1)
    @EqualsAndHashCode.Include
    @Column(name = "id")
    private Long id;
    @Column(name = "entity_id")
    private Long entityId;
    @Column(name = "entity_type")
    private String entityClass;
    @Column(name = "audit_message")
    private String auditMessage;
    @Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime createdAt;
}
