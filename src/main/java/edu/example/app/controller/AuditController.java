package edu.example.app.controller;

import edu.example.app.dto.audit.AuditResponseDto;
import edu.example.app.dto.audit.FilterAuditDto;
import edu.example.app.entity.ClassNames;
import edu.example.app.entity.security.UserPermission;
import edu.example.app.entity.specification.AuditSpecification;
import edu.example.app.mapper.AuditMapper;
import edu.example.app.service.domain.AuditService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Objects;

@RequiredArgsConstructor
@Controller
@UserPermission
public class AuditController {


    private final AuditService auditService;
    private final AuditMapper auditMapper;

    @GetMapping

    public Page<AuditResponseDto> getAudit(@Valid FilterAuditDto filterAuditDto)  {

        var specification = AuditSpecification
                .hasEntityClass(Objects.isNull(filterAuditDto.getClassName())
                        ? null:  ClassNames.valueOf(filterAuditDto.getClassName()))
                .and(AuditSpecification.hasEntityId(filterAuditDto.getEntityId()));

        var pageRequest = PageRequest.of(filterAuditDto.getPageNumber(), filterAuditDto.getPageSize());
        return auditService.getAudits(specification, pageRequest).map(auditMapper::toAuditResponseDto);
    }
}
