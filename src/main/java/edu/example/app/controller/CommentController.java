package edu.example.app.controller;

import edu.example.app.dto.comment.CommentResponseDto;
import edu.example.app.dto.comment.CreateCommentDto;
import edu.example.app.dto.comment.FilterCommentDto;
import edu.example.app.dto.comment.UpdateCommentDto;
import edu.example.app.entity.CommentEntity;
import edu.example.app.entity.security.TechnicalSupportPermission;
import edu.example.app.entity.security.UserPermission;
import edu.example.app.entity.specification.CommentSpecifications;
import edu.example.app.mapper.CommentMapper;
import edu.example.app.service.domain.CommentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping("/comment")
@CrossOrigin
@Tag(name = "Контроллер комментариев", description = "есть возможность создавать, удалять, " +
        "брать один комментарий и брать все комменты одного поста")
public class CommentController {

    private final CommentService commentService;
    private final CommentMapper commentMapper;

    @PostMapping
    @Operation(description = "Создать комментарий")
    @UserPermission
    public void createComment(@RequestBody @Valid CreateCommentDto createCommentDto, Principal principal) {
        commentService.createComment(commentMapper.toEntity(createCommentDto, principal));
    }

    @DeleteMapping("/{id}")
    @TechnicalSupportPermission
    @Operation(description = "Удалить комментарий по id")
    public void deleteComment(@PathVariable("id") Long id) {
        commentService.deleteComment(id);
    }

    @GetMapping("/{id}")
    @Operation(description = "Вытащить комментарий по id")
    public CommentResponseDto getComment(@PathVariable("id") Long id){
        CommentEntity comment = commentService.getResponseComment(id);
        return commentMapper.toCommentResponseDto(comment);
    }

    @GetMapping
    @Operation(description = "Вытащить все комментарии данного поста или родительского комментария")
    public Page<CommentResponseDto> getComments(@Valid FilterCommentDto filterCommentDto) {
        var specification = CommentSpecifications
                .createAfter(filterCommentDto.getCreateAfter())
                .and(CommentSpecifications.hasPost(filterCommentDto.getPostId()))
                .and(CommentSpecifications.hasParent(filterCommentDto.getParentId()));
        var pageRequest = PageRequest.of(filterCommentDto.getPageNumber(), filterCommentDto.getPageSize());
        return commentService.getResponseComments(specification, pageRequest).map(commentMapper::toCommentResponseDto);
    }
    
    @PutMapping("/{id}")
    @UserPermission
    @Operation(description = "Обновить комментарий по id")
    public void updateComment(@PathVariable("id") Long id, @RequestBody @Valid UpdateCommentDto updateCommentDto, Principal principal) {
         var oldComment = commentService.getComment(id);
         if (!Objects.equals(oldComment.getAuthor().getUsername(), principal.getName())) {
             throw new AccessDeniedException("Вы можете редактировать только свои комментарии");
         }
         commentService.updateComment(id, updateCommentDto.getText());
    }
}