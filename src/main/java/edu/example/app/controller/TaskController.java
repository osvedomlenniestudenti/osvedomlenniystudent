package edu.example.app.controller;

import edu.example.app.dto.task.CreateTaskDto;
import edu.example.app.dto.task.FilterTaskDto;
import edu.example.app.dto.task.TaskResponseDto;
import edu.example.app.entity.TaskEntity;
import edu.example.app.entity.security.AdminPermission;
import edu.example.app.entity.specification.TaskSpecification;
import edu.example.app.mapper.TaskMapper;
import edu.example.app.service.domain.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequiredArgsConstructor
@RequestMapping("/task")
@CrossOrigin(value = "*")
@Tag(name = "Контроллер заданий", description = "Есть возможность создавать, удалять, " +
        "брать одно задание и брать все задания одного предмета")
public class TaskController {

    private final TaskService taskService;
    private final TaskMapper taskMapper;

    @PostMapping
    @AdminPermission
    @Operation(description = "Создать задание")
    public TaskResponseDto createTask(@RequestBody @Valid CreateTaskDto createTaskDto, Principal principal){
        TaskEntity task = taskService.createTask(taskMapper.toEntity(createTaskDto, principal));

        return taskMapper.toTaskResponseDto(task);
    }

    @DeleteMapping("/{id}")
    @AdminPermission
    @Operation(description = "Удалить задание по id")
    public void deleteTask(@PathVariable("id") Long id){

        taskService.deleteTask(id);
    }

    @GetMapping("/{id}")
    @Operation(description = "Вытащить задание по id")
    public TaskResponseDto getTask(@PathVariable("id") Long id){
        TaskEntity task = taskService.getTask(id);
        return taskMapper.toTaskResponseDto(task);
    }


    @GetMapping
    @Operation(description = "Вытащить все задание данного предмета")
    public Page<TaskResponseDto> getComments(@Valid FilterTaskDto filterTaskDto) {
        var specification = TaskSpecification.createAfter(filterTaskDto.getCreateAfter())
                .and(TaskSpecification.hasSubjectId(filterTaskDto.getSubjectId()))
                .and(TaskSpecification.notFeed());
        var pageRequest = PageRequest.of(filterTaskDto.getPageNumber(), filterTaskDto.getPageSize());
        return taskService.getTasks(specification, pageRequest).map(taskMapper::toTaskResponseDto);
    }
}
