package edu.example.app.controller;

import edu.example.app.dto.post.*;
import edu.example.app.entity.PostEntity;
import edu.example.app.entity.RoleEntity;
import edu.example.app.entity.UserEntity;
import edu.example.app.entity.security.AdminPermission;
import edu.example.app.entity.security.UserPermission;
import edu.example.app.entity.specification.PostSpecifications;
import edu.example.app.mapper.PostMapper;
import edu.example.app.service.domain.PostService;
import edu.example.app.service.domain.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.naming.NoPermissionException;
import java.security.Principal;

import static edu.example.app.entity.TaskEntity.FEED_ID;
import static edu.example.app.entity.security.Roles.ADMIN;
import static edu.example.app.entity.security.Roles.MODERATOR;

@RestController
@RequiredArgsConstructor
@RequestMapping("/post")
@Tag(name = "Контроллер постов", description = "Есть возможность создавать, удалять, " +
        "брать один пост и брать все посты одного задания")
public class PostController {

    private final PostService postService;
    private final UserService userService;
    private final PostMapper postMapper;

    @PostMapping
    @UserPermission
    @Operation(description = "Создать пост")
    public PostResponseDto createPost(@RequestBody @Valid CreatePostDto createPostDto, Principal principal)
            throws NoPermissionException {
        UserEntity user = userService.getUserByUsername(principal.getName());
        if(createPostDto.getTaskId() == Long.parseLong(FEED_ID)){
            checkRight(user);
        }
        PostEntity post = postService.createPost(postMapper.toEntity(createPostDto, principal));
        return postMapper.toPostResponseDto(post);
    }

    @DeleteMapping("/{id}")
    @AdminPermission
    @Operation(description = "Удалить пост по id")
    public void deletePost(@PathVariable("id") Long id){
        postService.deletePost(id);
    }

    @GetMapping("/{id}")
    @Operation(description = "Вытащить пост по id")
    public PostResponseDto getPost(@PathVariable("id") Long id){
        PostEntity post = postService.getPost(id);
        return postMapper.toPostResponseDto(post);
    }

    @GetMapping
    @Operation(description = "Вытащить все посты данного задания")
    public Page<PostResponseDto> getPosts(@Valid FilterPostDto filterPostDto) {
        var specification = PostSpecifications.createAfter(filterPostDto.getCreateAfter())
                .and(PostSpecifications.hasTask(filterPostDto.getTaskId()));
        var pageRequest = PageRequest.of(filterPostDto.getPageNumber(), filterPostDto.getPageSize());
        return postService.getPosts(specification, pageRequest).map(postMapper::toPostResponseDto);
    }

    private void checkRight(UserEntity user) throws NoPermissionException {
        boolean noRight = true;
        for(RoleEntity roleEntity : user.getRoles()){
            if (roleEntity.getName().equals(ADMIN) || roleEntity.getName().equals(MODERATOR)) {
                noRight = false;
                break;
            }
        }
        if(noRight){
            throw new NoPermissionException("You have no right");
        }
    }
}
