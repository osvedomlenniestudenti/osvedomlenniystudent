package edu.example.app.controller;

import edu.example.app.dto.user.UserResponseDto;
import edu.example.app.entity.security.AdminPermission;
import edu.example.app.mapper.UserMapper;
import edu.example.app.service.domain.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/admin")
@AdminPermission
public class AdminController {

    private final UserMapper userMapper;
    private final UserService userService;

    @GetMapping("/profile")
    public List<UserResponseDto> adminProfile() {
        return userService
                .getAllUsers()
                .stream()
                .map(userMapper::toUserResponseDto)
                .toList();
    }

    @PostMapping("/add/{username}")
    public UserResponseDto addModerator(@PathVariable("username") String username) {
        var user = userService.getUserByUsername(username);
        user = userService.addModeratorRole(user);
        return userMapper.toUserResponseDto(user);
    }
}
