package edu.example.app.controller;

import edu.example.app.dto.subject.CreateSubjectDto;
import edu.example.app.dto.subject.FilterSubjectDto;
import edu.example.app.dto.subject.SubjectResponseDto;
import edu.example.app.entity.SubjectEntity;
import edu.example.app.entity.security.AdminPermission;
import edu.example.app.entity.specification.SubjectSpecification;
import edu.example.app.mapper.SubjectMapper;
import edu.example.app.service.domain.SubjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequiredArgsConstructor
@RequestMapping("/subject")
@Tag(name = "Контроллер предметов", description = "Есть возможность создавать, удалять, " +
        "брать один предмет и брать все предметы одного курса")
public class SubjectController {

    private final SubjectService subjectService;
    private final SubjectMapper subjectMapper;

    @PostMapping
    @AdminPermission
    @Operation(description = "Создать предмет")
    public SubjectResponseDto createSubject(@RequestBody @Valid CreateSubjectDto createSubjectDto, Principal principal) {

        var subject = subjectMapper.toEntity(createSubjectDto, principal);
        subject = subjectService.createSubject(subject);
        return subjectMapper.toSubjectResponseDto(subject);
    }

    @DeleteMapping("/{id}")
    @AdminPermission
    @Operation(description = "Удалить предмет по id")
    public void deleteSubject(@PathVariable("id") Long id) {
        subjectService.deleteSubject(id);
    }

    @GetMapping("/{id}")
    @Operation(description = "Вытащить предмет по id")
    public SubjectResponseDto getSubject(@PathVariable("id") Long id) {
        SubjectEntity subject = subjectService.getSubject(id);
        return subjectMapper.toSubjectResponseDto(subject);
    }

    @GetMapping
    @Operation(description = "Вытащить все предметы по номеру курса")
    public Page<SubjectResponseDto> getSubjects(@Valid FilterSubjectDto filterSubjectDto) {
        var specification = SubjectSpecification.hasCourse(filterSubjectDto.getCourse())
                .and(SubjectSpecification.notFeed());
        var pageRequest = PageRequest.of(filterSubjectDto.getPageNumber(), filterSubjectDto.getPageSize());
        return subjectService.getSubjects(specification, pageRequest).map(subjectMapper::toSubjectResponseDto);
    }
}
