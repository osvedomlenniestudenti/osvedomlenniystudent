package edu.example.app.controller;

import edu.example.app.dto.user.CreateUserDto;
import edu.example.app.dto.user.LoginUserDto;
import edu.example.app.dto.user.UserResponseDto;
import edu.example.app.mapper.UserMapper;
import edu.example.app.service.domain.UserService;
import edu.example.app.util.AuthUtil;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
@CrossOrigin(value = "*")
public class AuthController {

    private final UserMapper userMapper;
    private final AuthUtil authUtil;
    private final UserService userService;

    @PostMapping("/register")
    @Operation(description = "Зарегистрировать пользователя")
    public UserResponseDto createUser(@RequestBody @Valid CreateUserDto createUserDto) {
        var user = userMapper.toEntity(createUserDto);
        user = userService.createUser(user);
        var loginUserDto = userMapper.toLoginDto(createUserDto);
        return authUtil.responseAuthUser(user, loginUserDto);
    }

    @PostMapping("/login")
    @Operation(description = "Залогинить пользователя")
    public UserResponseDto login(@RequestBody LoginUserDto loginUserDto) {
        var user = userService.getUserByUsername(loginUserDto.getName());
        return authUtil.responseAuthUser(user, loginUserDto);
    }
}
