package edu.example.app.controller;

import edu.example.app.entity.FilePropertyEntity;
import edu.example.app.entity.security.UserPermission;
import edu.example.app.service.domain.FileService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/file")
@Tag(name = "Контроллер файлов", description = "Есть загружать файл, брать файл по имени")
public class FileController {

    private final FileService fileService;

    @PostMapping
    @UserPermission
    public FilePropertyEntity uploadFile(@RequestParam("file") MultipartFile file)
            throws IOException {
        return fileService.saveFile(file);
    }

    @GetMapping
    public Resource loadFileByName(@RequestParam("name") String name)
            throws MalformedURLException, FileNotFoundException {
        return fileService.getFileByName(name);
    }
}
