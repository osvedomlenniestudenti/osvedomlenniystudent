package edu.example.app.controller;

import edu.example.app.dto.user.ProfileUserResponseDto;
import edu.example.app.dto.user.UpdateUserProfileDto;
import edu.example.app.entity.UserEntity;
import edu.example.app.entity.security.AdminPermission;
import edu.example.app.entity.security.UserPermission;
import edu.example.app.mapper.UserMapper;
import edu.example.app.service.domain.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.naming.NoPermissionException;
import java.security.Principal;


@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@CrossOrigin(value = "*")
@Tag(name = "Контроллер пользователей", description = "Есть возможность регистрироваться и логиниться для пользователя")
public class UserController {

    private final UserMapper userMapper;
    private final UserService userService;

    @GetMapping("/info/{username}")
    public ProfileUserResponseDto userInfo(@PathVariable("username") String username) {
        UserEntity user = userService.getUserByUsername(username);
        if (!user.isEnabled() || user.isHidden()) {
            return userMapper.toHiddenProfileUserResponseDto(user);
        }
        return userMapper.toProfileUserResponseDto(user);
    }

    @GetMapping("/profile")
    @UserPermission
    public ProfileUserResponseDto userProfile(Principal principal) {
        UserEntity user = userService.getUserByUsername(principal.getName());
        return userMapper.toProfileUserResponseDto(user);
    }

    @PutMapping("/profile")
    @UserPermission
    public ProfileUserResponseDto updateUserProfile(@RequestBody @Valid UpdateUserProfileDto updateUserDto, Principal principal) throws NoPermissionException {
        String dtoUserName = updateUserDto.getName();
        String principalUserName = principal.getName();
        if (!dtoUserName.equals(principalUserName)) {
            throw new NoPermissionException();
        }

        var user = userMapper.toEntity(updateUserDto);
        user = userService.updateProfile(user);
        return userMapper.toProfileUserResponseDto(user);
    }

    @DeleteMapping("/{username}")
    @AdminPermission
    public void deleteUser(@PathVariable("username") String username) {
        userService.deactivateByUsername(username);
    }
}
