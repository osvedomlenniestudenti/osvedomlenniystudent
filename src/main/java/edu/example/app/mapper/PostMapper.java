package edu.example.app.mapper;

import edu.example.app.dto.post.CreatePostDto;
import edu.example.app.dto.post.PostResponseDto;
import edu.example.app.entity.PostEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.security.Principal;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface PostMapper {

    @Mapping(target = "author", qualifiedByName = "toUserDto")
    PostResponseDto toPostResponseDto(PostEntity post);

    @Mapping(source = "createPostDto.taskId", target = "task.id")
    @Mapping(source = "principal.name", target = "author.name")
    PostEntity toEntity(CreatePostDto createPostDto, Principal principal);
}
