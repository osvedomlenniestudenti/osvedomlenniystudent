package edu.example.app.mapper;

import edu.example.app.dto.comment.CommentResponseDto;
import edu.example.app.dto.comment.CreateCommentDto;
import edu.example.app.entity.CommentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.security.Principal;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface CommentMapper {

    @Mapping(target = "author", qualifiedByName = "toUserDto")
    CommentResponseDto toCommentResponseDto(CommentEntity comment);

    @Mapping(source = "createCommentDto.postId", target = "post.id")
    @Mapping(source = "createCommentDto.parentId", target = "parent.id")
    @Mapping(source = "principal.name", target = "author.name")
    CommentEntity toEntity(CreateCommentDto createCommentDto, Principal principal);

}
