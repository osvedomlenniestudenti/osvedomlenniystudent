package edu.example.app.mapper;

import java.security.Principal;

import edu.example.app.dto.subject.CreateSubjectDto;
import edu.example.app.dto.subject.SubjectResponseDto;
import edu.example.app.entity.SubjectEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface SubjectMapper {
    @Mapping(target = "author", qualifiedByName = "toUserDto")
    SubjectResponseDto toSubjectResponseDto(SubjectEntity subject);


    @Mapping(source = "createSubjectDto.name", target = "name")
    @Mapping(source = "principal.name", target = "author.name")
    SubjectEntity toEntity(CreateSubjectDto createSubjectDto, Principal principal);
}