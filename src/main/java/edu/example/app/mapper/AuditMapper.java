package edu.example.app.mapper;

import edu.example.app.dto.audit.AuditResponseDto;
import edu.example.app.entity.AuditEntity;
import edu.example.app.entity.CommentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")

public interface AuditMapper {

    @Mapping(target = "auditMessage", source = "comment.text")
    @Mapping(target = "entityClass", expression = "java(edu.example.app.entity.ClassNames.CommentEntity.name)")
    @Mapping(target = "entityId", source = "id")
    AuditEntity toEntity(CommentEntity comment);

    AuditResponseDto toAuditResponseDto(AuditEntity audit);
}
