package edu.example.app.mapper;

import java.security.Principal;

import edu.example.app.dto.task.CreateTaskDto;
import edu.example.app.dto.task.TaskResponseDto;
import edu.example.app.entity.TaskEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface TaskMapper {

    @Mapping(target = "author", qualifiedByName = "toUserDto")
    TaskResponseDto toTaskResponseDto(TaskEntity task);

    @Mapping(source = "createTaskDto.subjectId", target = "subject.id")
    @Mapping(source = "principal.name", target = "author.name")
    TaskEntity toEntity(CreateTaskDto createTaskDto, Principal principal);

}
