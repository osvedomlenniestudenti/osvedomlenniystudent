package edu.example.app.mapper;

import edu.example.app.dto.user.*;
import edu.example.app.entity.RoleEntity;
import edu.example.app.entity.UserEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(source = "password", target = "passwordHash")
    UserEntity toEntity(CreateUserDto createUserDto);

    UserEntity toEntity(UpdateUserProfileDto updateUserDto);

    LoginUserDto toLoginDto(CreateUserDto createUserDto);

    @Named(value = "toUserDto")
    UserResponseDto toUserResponseDto(UserEntity user);

    @Mapping(source = "roles", target = "roles")
    ProfileUserResponseDto toProfileUserResponseDto(UserEntity user);


    default List<String> roleName(Set<RoleEntity> roles) {
        if (roles == null || roles.isEmpty()) {
            return null;
        }
        return roles.stream().map(RoleEntity::getName).collect(Collectors.toList());
    }
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "name", source = "name")
    @Mapping(target = "hidden", source = "hidden")
    ProfileUserResponseDto toHiddenProfileUserResponseDto(UserEntity user);
}
