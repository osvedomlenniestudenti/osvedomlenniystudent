package edu.example.app.util;

import edu.example.app.dto.user.LoginUserDto;
import edu.example.app.dto.user.UserResponseDto;
import edu.example.app.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AuthUtil {

    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;

    public UserResponseDto responseAuthUser(UserEntity user, LoginUserDto userDto) throws BadCredentialsException {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userDto.getName(), userDto.getPassword())
        );
        String token = jwtUtil.createToken(user);
        return new UserResponseDto(userDto.getName(), token);
    }
}
