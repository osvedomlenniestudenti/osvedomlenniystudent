# studentHelp

## Общая информация
studentHelp - приложения для помощи студентам, позволяющее обмениваться файлами и сообщениями

## Интеграции 

studentHelp - rest приложение зависящее от исключительно от базы данных

От контрактов studentHelp зависит только studentHelpFront


## Подготовка к разработки
Для разработки потребуются следующий инструменты: npm, IntelliJ IDEA

    Получение npm: npm входит в пакет node - https://nodejs.org/en/download
    Получение IntelliJ IDEA: скачайте и установите с сайта https://www.jetbrains.com/idea/
## Начало разработки

Для начала разработки скопируйте локально studentHelp

    git clone https://gitlab.com/osvedomlenniestudenti/osvedomlenniystudent.git
Для заупсука studentHelp  необходимо предворительно запустить базу данных для этого выполнити
    
    docker-compose -f ./src/main/resources/db.yml up
После чего можете запускать studentHelp
## Ход разработки
Для распределения задач по проекту используется bitrix taskmanager https://b24-cbjkm8.bitrix24.ru/workgroups/group/1/tasks/

Вступите в проект и дождитесь получения задачи

После создайте ветку от init в формате

    Номер задчи: название задачи

После выполнение задачи необходимо сделать push в gitlab и 
создать mr к ветке init на gitlab сделать это можно средствами интерфейса gitlab

##  Merge request
После создание mp начнется стадия ci она состоит из одной стадии test

    В стадди test запускаются все тесты, вычислеяется покрытие 
    и проверяется codestyle.
В случае провала ci mp не может быть влит.

После прохождения ci для вливания необходимо получить хотя бы один approve

После вливание исходная ветка должна быть удалена, а связаная задача помечена как решеная

## Запуск клиента

Для запуска клиента необходим локально скопировать studentHelpFront

    git clone https://gitlab.com/osvedomlenniestudenti/osved_student_frontend.git

Затем из папки с проектом выполните команду

    npm start

После чего client должен запуститься